using System.Linq;
using Eye2.Data.Context;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Eye2.Data.Repositories
{
    public class PerfilRepository : BaseRepository<PerfPerfil, DbPostgresContext>, IPerfilRepository
    {
        public PerfilRepository(DbPostgresContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
        {
        }
    }
}