using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Entity;
using Eye2.Domain.DTO;
using Microsoft.EntityFrameworkCore;
using Eye2.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage;

namespace Eye2.Data.Repositories
{
    public class UsuariosRepository : BaseRepository<UsuaUsuario, DbPostgresContext>, IUsuarioRepository
    {
        public UsuariosRepository(DbPostgresContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
        {
        }
        public Task<UsuaUsuario> GetByEmailAsync(string email)
        {
            return dbContext.UsuaUsuario.Include(u => u.PerfPerfil).SingleOrDefaultAsync(u => u.UsuaEmail.Equals(email));
        }
        public async Task<bool> HasEmailAsync(string email)
        {
            var exists = await GetByEmailAsync(email);
            return exists != null;
        }
        public IDbContextTransaction BeginTransaction()
        {
            return base.dbContext.Database.BeginTransaction();
        }
    }
}