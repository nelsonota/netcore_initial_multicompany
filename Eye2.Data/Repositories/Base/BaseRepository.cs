﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using Eye2.Data.Context;
using Eye2.Domain.Entity;
using Eye2.Domain.Exceptions;
using Eye2.Domain.Interface.Repositories;
using Eye2.Domain.Interfaces.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Eye2.Data.Repositories
{
    public class BaseRepository<TEntity, TDbContext> : IBaseRepository<TEntity>
        where TEntity : IEntity
        where TDbContext : DbContext
    {
        public readonly TDbContext dbContext;
        public readonly IHttpContextAccessor _httpContextAccessor;

        public BaseRepository(TDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            this.dbContext = context;
            this._httpContextAccessor = httpContextAccessor;
        }

        public IQueryable<TEntity> AsQueryable()
        {
            // TODO: TRATAR empr_codigo quando for UsuaUsuario
            IQueryable<TEntity> query = dbContext.Set<TEntity>().AsQueryable();
            query = ApplyFilter(query);
            return query;
        }

        private IQueryable<TEntity> ApplyFilter(IQueryable<TEntity> query)
        {
            //if T is ITagableEntity filter by token somehow
            if(typeof(IHasAccount).IsAssignableFrom(typeof(TEntity)) || typeof(TEntity) == typeof(UsuaUsuario))
            {
                string emprCodigo = _httpContextAccessor.HttpContext?.Session.GetString("EmprCodigo") ?? "";
                if (string.IsNullOrEmpty(emprCodigo)) throw new FilterException("EmprCodigo does not exists");

                // return query.Cast<IHasAccount>().Where(q => q.EmprCodigoFK == long.Parse(emprCodigo)).Cast<TEntity>();
                if (typeof(TEntity) == typeof(UsuaUsuario))
                {
                    // Cast para IQueryable<UsuaUsuario>
                    var userQuery = query as IQueryable<UsuaUsuario>;
                    if (userQuery != null)
                    {
                        long emprCodigoLong = long.Parse(emprCodigo);

                        // Faz o inner join com PerfPerfil e valida EmprCodigoFK
                        userQuery = userQuery
                            .Include(u => u.PerfPerfil) // Inclui PerfPerfil
                            .Where(u => u.PerfPerfil.EmprCodigoFK == emprCodigoLong);

                        return userQuery as IQueryable<TEntity>; // Retorna a query como IQueryable<TEntity>
                    } else {
                        throw new FilterException("Filter error on EmprCodigo of UsuaUsuario");
                    }
                }
                else
                {
                    // Filtra por EmprCodigoFK para outras entidades que implementam IHasAccount
                    return query.Cast<IHasAccount>().Where(q => q.EmprCodigoFK == long.Parse(emprCodigo)).Cast<TEntity>();
                }
            }
            else
            {
                return query;
            }
        }

        public async Task<TEntity> GetByIdAsync(long Id, bool noTracking = false)
        {
            var entity = await dbContext.Set<TEntity>().FindAsync(Id);

            if (noTracking && entity != null)
            {
                dbContext.Entry(entity).State = EntityState.Detached;
            }

            return entity;
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            dbContext.Set<TEntity>().Add(entity);
            await dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> UpdateAsync(TEntity entity, bool disconectedScenario = true)
        {
            if (disconectedScenario)
                dbContext.Set<TEntity>().Update(entity);
            await dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task<int> DeleteAsync(TEntity entity)
        {
            dbContext.Set<TEntity>().Remove(entity);
            return await dbContext.SaveChangesAsync();
        }

        public async Task<List<TEntity>> UpdateAsync(List<TEntity> listEntity, bool disconectedScenario = true)
        {
            if (disconectedScenario)
                dbContext.Set<TEntity>().UpdateRange(listEntity);

            await dbContext.SaveChangesAsync();

            return listEntity;
        }

        public async Task<int> DeleteAsync(List<TEntity> entity)
        {
            dbContext.Set<TEntity>().RemoveRange(entity);
            return await dbContext.SaveChangesAsync();
        }
    }
}
