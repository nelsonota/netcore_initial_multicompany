using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Eye2.Data.Repositories
{
    public class KeycloakRepository: IKeycloakRepository
    {
        private readonly HttpClient _httpClient;
        private readonly string _realm;
        private readonly string _clientId;
        private readonly string _clientSecret;
        private readonly string _adminUsername;
        private readonly string _adminPassword;
        private readonly string _keycloakUrl = "https://localhost:8443";
        private readonly string _keycloakApiUrl;
        private string _token;
        public KeycloakRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _realm = "viperson";
            _clientId = "dentist-api";
            _clientSecret = "e3yWjL9n0Dtp202wnzW0qGNeVgX3OPex";
        }

        public async Task AuthenticateAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_keycloakUrl}/realms/{_realm}/protocol/openid-connect/token");

            var keyValues = new[]
            {
                new KeyValuePair<string, string>("client_id", _clientId),
                new KeyValuePair<string, string>("client_secret", _clientSecret),
                // new KeyValuePair<string, string>("username", _adminUsername),
                // new KeyValuePair<string, string>("password", _adminPassword),
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            };

            request.Content = new FormUrlEncodedContent(keyValues);
            
            var response = await _httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var tokenResponse = JObject.Parse(content);
            _token = tokenResponse["access_token"].ToString();
        }
        
        public async Task CreateUser(KeycloakUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            try
            {
                await AuthenticateAsync();

                // Faça a chamada POST para criar o usuário
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                var response = await _httpClient.PostAsJsonAsync($"{_keycloakUrl}/admin/realms/{_realm}/users", user);

                // Verifique se a resposta foi bem-sucedida
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                // Lide com exceções aqui (registre, relance, etc.)
                Console.WriteLine($"Erro ao criar usuário no Keycloak: {ex.Message}");
                throw ex;
            }
        }

        public async Task<string> GetUserIdByEmailAsync(string email)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_keycloakUrl}/admin/realms/{_realm}/users?email={email}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);

            var response = await _httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var users = JArray.Parse(content);

            if (users.Count == 0)
            {
                throw new Exception("User not found");
            }

            return users[0]["id"].ToString();
        }

        public async Task DeleteUser(string email)
        {
            await AuthenticateAsync();
            var userId = await GetUserIdByEmailAsync(email);
            var request = new HttpRequestMessage(HttpMethod.Delete, $"{_keycloakUrl}/admin/realms/{_realm}/users/{userId}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);

            var response = await _httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }
    }
}