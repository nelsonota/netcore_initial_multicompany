using System.Linq;
using Eye2.Data.Context;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage;

namespace Eye2.Data.Repositories
{
    public class EmpresaRepository : BaseRepository<EmprEmpresa, DbPostgresContext>, IEmpresaRepository
    {
        public EmpresaRepository(DbPostgresContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
        {
        }
        public bool HasDocumento(string documento)
        {
            var exists = dbContext.EmprEmpresa.SingleOrDefault(e => e.EmprDocumento.Equals(documento));
            return exists != null;
        }
        public IDbContextTransaction BeginTransaction()
        {
            return base.dbContext.Database.BeginTransaction();
        }
    }
}