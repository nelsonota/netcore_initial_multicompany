using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace Eye2.Data.Repositories
{
    public class InMemEmpresasRepository : IEmpresaRepository
    {

        private readonly List<EmprEmpresa> empresas = new()
        {
            new EmprEmpresa { EmprCodigo = 1, EmprDocumento = "11111111111", EmprRazaoSocial = "Nelson", Created = System.DateTime.UtcNow },
            new EmprEmpresa { EmprCodigo = 2, EmprDocumento = "22222222222", EmprRazaoSocial = "Leandra", Created = System.DateTime.UtcNow },
            new EmprEmpresa { EmprCodigo = 3, EmprDocumento = "33333333333", EmprRazaoSocial = "Antonia", Created = System.DateTime.UtcNow },
            new EmprEmpresa { EmprCodigo = 4, EmprDocumento = "44444444444", EmprRazaoSocial = "Alice", Created = System.DateTime.UtcNow },
        };

        private int _seqId = 4;

        public InMemEmpresasRepository()
        {
        }

        public IDbContextTransaction BeginTransaction()
        {
            throw new System.NotImplementedException();
        }

        public Task<EmprEmpresa> CreateAsync(EmprEmpresa empresa)
        {
            _seqId ++;
            empresa.EmprCodigo = _seqId;
            empresas.Add(empresa);
            return Task.FromResult(empresa);
        }

        public Task<int> DeleteAsync(EmprEmpresa empresa)
        {
            var index = empresas.FindIndex(existingItem => existingItem.EmprCodigo == empresa.EmprCodigo);
            empresas.RemoveAt(index);
            return Task.FromResult(1);
        }

        public Task<int> DeleteAsync(List<EmprEmpresa> entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<EmprEmpresa> GetByIdAsync(long Id, bool noTracking = false)
        {
            EmprEmpresa empresa = empresas.Where(empresa => empresa.EmprCodigo == Id).SingleOrDefault();
            return Task.FromResult(empresa);
        }

        public IEnumerable<EmprEmpresa> GetEmpresasAsync()
        {
            return empresas.AsEnumerable();
        }

        public bool HasDocumento(string documento)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<EmprEmpresa> AsQueryable()
        {
            return empresas.AsQueryable();
        }

        public Task<EmprEmpresa> UpdateAsync(EmprEmpresa empresa, bool disconectedScenario = true)
        {
            var index = empresas.FindIndex(existingItem => existingItem.EmprCodigo == empresa.EmprCodigo);
            empresas[index] = empresa;
            return Task.FromResult(empresa);
        }

        public Task<List<EmprEmpresa>> UpdateAsync(List<EmprEmpresa> listEntity, bool disconectedScenario = true)
        {
            throw new System.NotImplementedException();
        }
    }
}