using System.Linq;
using Eye2.Data.Context;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Http;

namespace Eye2.Data.Repositories
{
    public class PerfObjectRepository : BaseRepository<PobjPerfObject, DbPostgresContext>, IPerfObjectRepository
    {
        public PerfObjectRepository(DbPostgresContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
        {
        }
    }
}