﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eye2.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        public InitialCreate()
        {
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                CREATE TABLE IF NOT EXISTS public.jour_journal
                (
                    jour_codigo bigserial NOT NULL,
                    jour_model character varying(100) NOT NULL,
                    jour_model_codigo bigint NOT NULL,
                    jour_json_data json NOT NULL,
                    jour_created timestamp with time zone NOT NULL,
                    jour_usua_codigo bigint NOT NULL,
                    jour_empr_codigo bigint NOT NULL,
                    PRIMARY KEY (jour_codigo)
                );
                
                CREATE TABLE IF NOT EXISTS public.jdet_journal_detail
                (
                    jdet_codigo bigserial NOT NULL,
                    jdet_jour_codigo bigint NOT NULL,
                    jdet_property character varying(100) NOT NULL,
                    jdet_prop_key character varying(100) NOT NULL,
                    jdet_old_value text NOT NULL,
                    jdet_value text NOT NULL,
                    PRIMARY KEY (jdet_codigo)
                );

                CREATE TABLE empr_empresa  ( 
                    empr_codigo      	bigserial NOT NULL,
                    empr_documento   	varchar(14) NOT NULL,
                    empr_razao_social	varchar(60) NOT NULL,
                    empr_created     	timestamp NOT NULL,
                    empr_modified    	timestamp NULL,
                    empr_deleted     	timestamp NULL,
                    PRIMARY KEY(empr_codigo),
                    UNIQUE(empr_documento)
                );

                CREATE TABLE perf_perfil  ( 
                    perf_codigo      	bigserial NOT NULL,
                    perf_nome   	    varchar(60) NOT NULL,
                    perf_empr_codigo    bigint NOT NULL,
                    perf_created     	timestamp NOT NULL,
                    perf_modified    	timestamp NULL,
                    perf_deleted     	timestamp NULL,
                    PRIMARY KEY(perf_codigo)
                );

                CREATE UNIQUE INDEX unique_perf_nome_empr_codigo_deleted_null ON perf_perfil (perf_nome, perf_empr_codigo) WHERE perf_deleted IS NULL;

                CREATE TABLE usua_usuario  ( 
                    usua_codigo     	bigserial NOT NULL,
                    usua_perf_codigo    bigint NOT NULL,
                    usua_email      	varchar(128) NOT NULL,
                    usua_nome       	varchar(60) NOT NULL,
                    usua_created    	timestamp NOT NULL,
                    usua_modified   	timestamp NULL,
                    usua_deleted    	timestamp NULL,
                    PRIMARY KEY(usua_codigo),
                    UNIQUE(usua_email)
                );

                CREATE UNIQUE INDEX unique_usua_email_deleted_null ON usua_usuario (usua_email) WHERE usua_deleted IS NULL;

                ALTER TABLE public.jdet_journal_detail
                    ADD FOREIGN KEY (jdet_jour_codigo)
                    REFERENCES public.jour_journal (jour_codigo)
                    NOT VALID;

                ALTER TABLE perf_perfil
                    ADD CONSTRAINT perf_empr
                    FOREIGN KEY(perf_empr_codigo)
                    REFERENCES empr_empresa(empr_codigo)
                    ON DELETE NO ACTION 
                    ON UPDATE NO ACTION;

                ALTER TABLE usua_usuario
                    ADD CONSTRAINT usua_perf
                    FOREIGN KEY(usua_perf_codigo)
                    REFERENCES perf_perfil(perf_codigo)
                    ON DELETE NO ACTION 
                    ON UPDATE NO ACTION;    
            ";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                DROP TABLE IF EXISTS public.usua_usuario CASCADE;
                DROP TABLE IF EXISTS public.perf_perfil CASCADE;
                DROP TABLE IF EXISTS public.empr_empresa CASCADE;
                DROP TABLE IF EXISTS public.jdet_journal_detail CASCADE;
                DROP TABLE IF EXISTS public.jour_journal CASCADE;   
            ";
            migrationBuilder.Sql(sp);
        }
    }
}
