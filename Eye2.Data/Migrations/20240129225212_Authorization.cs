﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Eye2.Data.Migrations
{
    /// <inheritdoc />
    public partial class Authorization : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                CREATE TABLE IF NOT EXISTS public.pobj_perf_object
                (
                    pobj_codigo bigserial NOT NULL,
                    pobj_perf_codigo bigint NOT NULL,
                    pobj_objeto varchar(512) NOT NULL,
                    pobj_created timestamp with time zone NOT NULL,
                    PRIMARY KEY (pobj_codigo)
                );
            ";

            migrationBuilder.Sql(sp);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                DROP TABLE IF EXISTS public.pobj_perf_object CASCADE;
            ";
            migrationBuilder.Sql(sp);
        }
    }
}
