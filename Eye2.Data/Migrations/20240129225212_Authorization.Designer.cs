﻿// <auto-generated />
using System;
using Eye2.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Eye2.Data.Migrations
{
    [DbContext(typeof(DbPostgresContext))]
    [Migration("20240129225212_Authorization")]
    partial class Authorization
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("Eye2.Domain.Entity.EmprEmpresa", b =>
                {
                    b.Property<long>("EmprCodigo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("empr_codigo");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("EmprCodigo"));

                    b.Property<DateTime?>("Created")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("empr_created");

                    b.Property<DateTime?>("Deleted")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("empr_deleted");

                    b.Property<string>("EmprDocumento")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("empr_documento");

                    b.Property<string>("EmprRazaoSocial")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("empr_razao_social");

                    b.Property<DateTime?>("Modified")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("empr_modified");

                    b.HasKey("EmprCodigo");

                    b.ToTable("empr_empresa", (string)null);
                });

            modelBuilder.Entity("Eye2.Domain.Entity.PerfPerfil", b =>
                {
                    b.Property<long>("PerfCodigo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("perf_codigo");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("PerfCodigo"));

                    b.Property<DateTime?>("Created")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("perf_created");

                    b.Property<DateTime?>("Deleted")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("perf_deleted");

                    b.Property<long>("EmprCodigoFK")
                        .HasColumnType("bigint")
                        .HasColumnName("perf_empr_codigo");

                    b.Property<DateTime?>("Modified")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("perf_modified");

                    b.Property<string>("PerfNome")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("perf_nome");

                    b.HasKey("PerfCodigo");

                    b.HasIndex("EmprCodigoFK");

                    b.ToTable("perf_perfil", (string)null);
                });

            modelBuilder.Entity("Eye2.Domain.Entity.UsuaUsuario", b =>
                {
                    b.Property<long>("UsuaCodigo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("usua_codigo");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("UsuaCodigo"));

                    b.Property<DateTime?>("Created")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("usua_created");

                    b.Property<DateTime?>("Deleted")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("usua_deleted");

                    b.Property<DateTime?>("Modified")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("usua_modified");

                    b.Property<string>("UsuaEmail")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("usua_email");

                    b.Property<string>("UsuaNome")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("usua_nome");

                    b.Property<long>("UsuaPerfCodigo")
                        .HasColumnType("bigint")
                        .HasColumnName("usua_perf_codigo");

                    b.Property<string>("UsuaRefreshToken")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("usua_refresh_token");

                    b.Property<DateTime>("UsuaRefreshTokenExpireTime")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("usua_token_expiry_time");

                    b.Property<string>("UsuaSenha")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("usua_senha");

                    b.HasKey("UsuaCodigo");

                    b.HasIndex("UsuaPerfCodigo");

                    b.ToTable("usua_usuario", (string)null);
                });

            modelBuilder.Entity("Eye2.Domain.Entity.PerfPerfil", b =>
                {
                    b.HasOne("Eye2.Domain.Entity.EmprEmpresa", "EmprEmpresa")
                        .WithMany("Perfis")
                        .HasForeignKey("EmprCodigoFK")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("EmprEmpresa");
                });

            modelBuilder.Entity("Eye2.Domain.Entity.UsuaUsuario", b =>
                {
                    b.HasOne("Eye2.Domain.Entity.PerfPerfil", "PerfPerfil")
                        .WithMany("Usuarios")
                        .HasForeignKey("UsuaPerfCodigo")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("PerfPerfil");
                });

            modelBuilder.Entity("Eye2.Domain.Entity.EmprEmpresa", b =>
                {
                    b.Navigation("Perfis");
                });

            modelBuilder.Entity("Eye2.Domain.Entity.PerfPerfil", b =>
                {
                    b.Navigation("Usuarios");
                });
#pragma warning restore 612, 618
        }
    }
}
