using Eye2.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Eye2.Data.Mappings
{
    internal class EmprEmpresaMap : IEntityTypeConfiguration<EmprEmpresa>
    {
        public void Configure(EntityTypeBuilder<EmprEmpresa> builder)
        {
            builder.ToTable("empr_empresa");

            builder.HasKey(e => e.EmprCodigo);

            builder.Property(e => e.EmprCodigo).HasColumnName("empr_codigo");
            builder.Property(e => e.EmprDocumento).HasColumnName("empr_documento");
            builder.Property(e => e.EmprRazaoSocial).HasColumnName("empr_razao_social");
            builder.Property(e => e.Created).HasColumnName("empr_created");
            builder.Property(e => e.Modified).HasColumnName("empr_modified");
            builder.Property(e => e.Deleted).HasColumnName("empr_deleted");

            builder.HasMany(p => p.Perfis).WithOne(e => e.EmprEmpresa).HasForeignKey(p => p.EmprCodigoFK);
       }
    }
}