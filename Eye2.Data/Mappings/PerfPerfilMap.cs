using Eye2.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Eye2.Data.Mappings
{
    internal class PerfPerfilMap : IEntityTypeConfiguration<PerfPerfil>
    {
        public void Configure(EntityTypeBuilder<PerfPerfil> builder)
        {
            builder.ToTable("perf_perfil");

            builder.HasKey(e => e.PerfCodigo);

            builder.Property(e => e.PerfCodigo).HasColumnName("perf_codigo");
            builder.Property(e => e.PerfNome).HasColumnName("perf_nome");
            builder.Property(e => e.EmprCodigoFK).HasColumnName("perf_empr_codigo");
            builder.Property(e => e.Created).HasColumnName("perf_created");
            builder.Property(e => e.Modified).HasColumnName("perf_modified");
            builder.Property(e => e.Deleted).HasColumnName("perf_deleted");

            // builder.HasMany(p => p.Usuarios).WithOne(e => e.PerfPerfil).HasForeignKey(p => p.UsuaPerfCodigo);
       }
    }
}