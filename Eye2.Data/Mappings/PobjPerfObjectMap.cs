using Eye2.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Eye2.Data.Mappings
{
    internal class PobjPerfObjectoMap : IEntityTypeConfiguration<PobjPerfObject>
    {
        public void Configure(EntityTypeBuilder<PobjPerfObject> builder)
        {
            builder.ToTable("pobj_perf_object");

            builder.HasKey(e => e.PobjCodigo);

            builder.Property(e => e.PobjCodigo).HasColumnName("pbj_codigo");
            builder.Property(e => e.PobjPerfCodigo).HasColumnName("pobj_perf_codigo");
            builder.Property(e => e.PobjObjeto).HasColumnName("pobj_objeto");
            builder.Property(e => e.Created).HasColumnName("pobj_created");

            // builder.HasOne(e => e.EmprEmpresa).WithMany(c => c.Usuarios).HasForeignKey(u => u.UsuaEmprCodigo);
       }
    }
}