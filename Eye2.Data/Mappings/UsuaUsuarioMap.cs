using Eye2.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Eye2.Data.Mappings
{
    internal class UsuaUsuarioMap : IEntityTypeConfiguration<UsuaUsuario>
    {
        public void Configure(EntityTypeBuilder<UsuaUsuario> builder)
        {
            builder.ToTable("usua_usuario");

            builder.HasKey(e => e.UsuaCodigo);

            builder.Property(e => e.UsuaCodigo).HasColumnName("usua_codigo");
            builder.Property(e => e.UsuaPerfCodigo).HasColumnName("usua_perf_codigo");
            builder.Property(e => e.UsuaNome).HasColumnName("usua_nome");
            builder.Property(e => e.UsuaEmail).HasColumnName("usua_email");
            // builder.Property(e => e.UsuaRefreshToken).HasColumnName("usua_refresh_token");
            // builder.Property(e => e.UsuaRefreshTokenExpireTime).HasColumnName("usua_token_expiry_time");
            // builder.Property(e => e.UsuaSenha).HasColumnName("usua_senha");
            builder.Property(e => e.Created).HasColumnName("usua_created");
            builder.Property(e => e.Modified).HasColumnName("usua_modified");
            builder.Property(e => e.Deleted).HasColumnName("usua_deleted");
            builder.HasOne(u => u.PerfPerfil).WithMany(p => p.Usuarios).HasForeignKey(u => u.UsuaPerfCodigo);
            // builder.HasOne(e => e.EmprEmpresa).WithMany(c => c.Usuarios).HasForeignKey(u => u.UsuaEmprCodigo);
       }
    }
}