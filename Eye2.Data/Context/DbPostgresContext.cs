using System;
using System.Security.Claims;
using Eye2.Data.Mappings;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Eye2.Data.Context
{
    public partial class DbPostgresContext : DbContext
    {

        public readonly IHttpContextAccessor httpContextAccessor;

        public DbPostgresContext(DbContextOptions<DbPostgresContext> options, IHttpContextAccessor httpContextAccessor)
            : base(options)
        {
            ChangeTracker.StateChanged += beforeUpdate;
            ChangeTracker.Tracked += beforeUpdate;
            this.httpContextAccessor = httpContextAccessor;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Habilitar o Sensitive Data Logging
            optionsBuilder.EnableSensitiveDataLogging();

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmprEmpresaMap());
            modelBuilder.ApplyConfiguration(new PerfPerfilMap());
            modelBuilder.ApplyConfiguration(new UsuaUsuarioMap());
            modelBuilder.ApplyConfiguration(new PobjPerfObjectoMap());

            // Aplicando filtros globais para ignorar entidades marcadas como deletadas
            modelBuilder.Entity<EmprEmpresa>().HasQueryFilter(e => e.Deleted == null);
            modelBuilder.Entity<PerfPerfil>().HasQueryFilter(p => p.Deleted == null);
            modelBuilder.Entity<UsuaUsuario>().HasQueryFilter(u => u.Deleted == null);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        private void beforeUpdate(object sender, EntityEntryEventArgs e)
        {
            if (e.Entry.Entity is IHasTimestamps) UpdateTimestamps(sender, e);
            if (e.Entry.Entity is IHasAccount) UpdateAccount(sender, e);
        }

        private void UpdateTimestamps(object sender, EntityEntryEventArgs e)
        {
            if (e.Entry.Entity is IHasTimestamps entityWithTimestamps)
            {
                switch (e.Entry.State)
                {
                    case EntityState.Deleted:
                        e.Entry.State = EntityState.Modified; // Cancela o delete para mudar para update
                        entityWithTimestamps.Deleted = DateTime.UtcNow;
                        // Console.WriteLine($"Stamped for delete: {e.Entry.Entity}");
                        break;
                    case EntityState.Modified:
                        entityWithTimestamps.Modified = DateTime.UtcNow;
                        // Console.WriteLine($"Stamped for update: {e.Entry.Entity}");
                        break;
                    case EntityState.Added:
                        entityWithTimestamps.Created = DateTime.UtcNow;
                        // Console.WriteLine($"Stamped for insert: {e.Entry.Entity}");
                        break;
                }
            }
        }

        private void UpdateAccount(object sender, EntityEntryEventArgs e)
        {
            if (e.Entry.Entity is IHasAccount entityWithAccount)
            {
                switch (e.Entry.State)
                {
                    case EntityState.Added:
                        string emprCodigo = httpContextAccessor.HttpContext?.Session.GetString("EmprCodigo") ?? "";
                        if (!string.IsNullOrEmpty(emprCodigo))
                            entityWithAccount.EmprCodigoFK = long.Parse(emprCodigo);
                        break;
                }
            }
        }

        public DbSet<EmprEmpresa> EmprEmpresa { get; set; }
        public DbSet<UsuaUsuario> UsuaUsuario { get; set; }
        public DbSet<PobjPerfObject> PobjPerfObject { get; set; }
    }
}
