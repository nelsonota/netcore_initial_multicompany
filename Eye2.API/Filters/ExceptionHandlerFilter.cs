﻿using System.Net;
using Eye2.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Eye2.API.Filters
{
    ///Filter Exception Attribute
    public class ExceptionHandlerFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionHandlerFilter> logger;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        public ExceptionHandlerFilter(ILogger<ExceptionHandlerFilter> logger)
        {
            this.logger = logger;
        }

        private void Log(bool isDebug, ExceptionContext context)
        {
            if (isDebug)
                logger.LogDebug(context.Exception, context.Exception.Message ?? context.Exception.InnerException.Message);
            else
                logger.LogError(context.Exception, context.Exception.Message ?? context.Exception.InnerException.Message);
        }

        /// <summary>
        /// filter exception
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(ExceptionContext context)
        {
            bool isDebug = true;

            if (context.Exception is ValidationException) // || context.Exception is BusinessException)
            {
                MountBadRequest(context);
            }
            // else if (context.Exception is AuthorizationException)
            // {
            //     MountUnauthorized(context);
            // }
            // else if (context.Exception is LoggedException)
            // {
            //     MountInternalServerError(context);
            // }
            else
            {
                isDebug = false;
                MountInternalServerError(context);
            }

            Log(isDebug, context);
        }

        private void MountResponse(ExceptionContext context, ProblemDetails obj)
        {
            context.HttpContext.Response.ContentType = "application/problem+json";
            context.HttpContext.Response.StatusCode = obj.Status.Value;

            context.Result = new JsonResult(obj);
        }

        private void MountUnauthorized(ExceptionContext context)
        {
            var obj = MountProblemDetails(context, (int)HttpStatusCode.Unauthorized);
            MountResponse(context, obj);
        }

        private void MountBadRequest(ExceptionContext context)
        {
            if (context.Exception is ValidationException exception)
            {
                var validationProblems = new ValidationProblemDetails(exception.Failures)
                {
                    Title = context.Exception.Message,
                    Status = (int)HttpStatusCode.BadRequest,
                    // Detail = context.Exception.StackTrace,
                    Instance = context.HttpContext.Request.Path
                };

                MountResponse(context, validationProblems);
            }
            else
            {
                var obj = new ProblemDetails()
                {
                    Title = context.Exception.Message,
                    Status = (int)HttpStatusCode.BadRequest,
                    // Detail = context.Exception.StackTrace,
                    Instance = context.HttpContext.Request.Path,
                };

                MountResponse(context, obj);
            }
        }

        private void MountInternalServerError(ExceptionContext context)
        {
            var obj = MountProblemDetails(context, (int)HttpStatusCode.InternalServerError);
            MountResponse(context, obj);
        }

        private ProblemDetails MountProblemDetails(ExceptionContext context, int statusCode) => new ProblemDetails()
        {
            Title = context.Exception.Message,
            Status = statusCode,
            Detail = context.Exception.StackTrace,
            Instance = context.HttpContext.Request.Path,
        };
    }
}
