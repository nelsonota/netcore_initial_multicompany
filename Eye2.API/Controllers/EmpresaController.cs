using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Eye2.Domain.Entity;
using Eye2.Domain.DTO;
using Microsoft.AspNetCore.Authorization;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Exceptions;

namespace Eye2.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class EmpresaController : BaseController
    {
        private readonly IEmpresaService _empresaService;

        public EmpresaController(IEmpresaService empresaService, IMapper mapper) : base(mapper)
        {
            _empresaService = empresaService;
        }

        // GET: api/Empresa
        [HttpGet]
        public ActionResult<IEnumerable<EmpresaDTO>> GetEmprEmpresa()
        {
            IEnumerable<EmprEmpresa> emprEmpresas = _empresaService.AsQueryable();
            IEnumerable<EmpresaDTO> empresas = mapper.Map<IEnumerable<EmprEmpresa>, IEnumerable<EmpresaDTO>>(emprEmpresas);

            return Ok(empresas);
        }

        // GET: api/Empresa/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmpresaDTO>> GetEmprEmpresaAsync(long id)
        {
            EmprEmpresa emprEmpresa = await _empresaService.GetByIdAsync(id);

            if (emprEmpresa == null)
            {
                return NotFound();
            }

            var empresa = mapper.Map<EmpresaDTO>(emprEmpresa);
            return Ok(empresa);
        }

        // PUT: api/Empresa/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmprEmpresaAsync(long id, EmprEmpresa emprEmpresa)
        {
            if (id != emprEmpresa.EmprCodigo)
            {
                return BadRequest();
            }

            EmprEmpresa existingItem = await _empresaService.GetByIdAsync(id);
            if (existingItem is null)
            {
                return NotFound();
            }

            existingItem.EmprRazaoSocial = emprEmpresa.EmprRazaoSocial;
            existingItem.EmprDocumento = emprEmpresa.EmprDocumento;

            try
            {
                EmprEmpresa result = await _empresaService.UpdateAsync(existingItem);
                return Ok(result);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        // POST: api/Empresa
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        // [HttpPost]
        // public ActionResult<EmpresaDTO> PostEmprEmpresa(NovoEmpresaDTO request)
        // {
        //     EmprEmpresa created = _empresaRepository.Create(request.toClass());
        //     var empresa = mapper.Map<EmpresaDTO>(created);
        //     return CreatedAtAction("GetEmprEmpresa", new { id = created.EmprCodigo }, empresa);
        // }

        // DELETE: api/Empresa/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmprEmpresaAsync(long id)
        {
            var emprEmpresa = await _empresaService.GetByIdAsync(id);
            if (emprEmpresa == null)
            {
                return NotFound();
            }
            try
            {
                await _empresaService.DeleteAsync(emprEmpresa);

                return NoContent();    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }
    }
}
