using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Eye2.Domain.Entity;
using Eye2.Domain.DTO;
using Microsoft.AspNetCore.Authorization;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Exceptions;

namespace Eye2.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsuarioController : BaseController
    {
        private readonly IUsuarioService _UsuarioService;

        public UsuarioController(IUsuarioService UsuarioService, IMapper mapper) : base(mapper)
        {
            _UsuarioService = UsuarioService;
        }

        // GET: api/Usuario
        [HttpGet]
        public ActionResult<IEnumerable<UsuarioDTO>> GetUsuaUsuario()
        {
            IEnumerable<UsuaUsuario> UsuaUsuarios = _UsuarioService.AsQueryable();
            IEnumerable<UsuarioDTO> Usuarios = mapper.Map<IEnumerable<UsuaUsuario>, IEnumerable<UsuarioDTO>>(UsuaUsuarios);
            return Ok(Usuarios);
        }

        // GET: api/Usuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UsuarioDTO>> GetUsuaUsuarioAsync(long id)
        {
            UsuaUsuario UsuaUsuario = await _UsuarioService.GetByIdAsync(id);

            if (UsuaUsuario == null)
            {
                return NotFound();
            }

            var Usuario = mapper.Map<UsuarioDTO>(UsuaUsuario);
            return Ok(Usuario);
        }

        // PUT: api/Usuario/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuaUsuario(long id, UsuarioDTO UsuarioDto)
        {
            if (id != UsuarioDto.codigo)
            {
                return BadRequest();
            }

            UsuaUsuario existingItem = await _UsuarioService.GetByIdAsync(id);
            if (existingItem is null)
            {
                return NotFound();
            }

            existingItem.UsuaNome = UsuarioDto.nome;
            existingItem.UsuaPerfCodigo = UsuarioDto.perfil.codigo;
            try
            {
                UsuaUsuario result = await _UsuarioService.UpdateAsync(existingItem);
                return Ok(result);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        // POST: api/Usuario
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<UsuarioDTO>> PostUsuaUsuarioAsync(NovoUsuarioDTO request)
        {
            try
            {
                UsuaUsuario created = await _UsuarioService.CreateAsync(request.toClass());
                var Usuario = mapper.Map<UsuarioDTO>(created);
                return CreatedAtAction("GetUsuaUsuario", new { id = created.UsuaCodigo }, Usuario);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        // DELETE: api/Usuario/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsuaUsuarioAsync(long id)
        {
            var UsuaUsuario = await _UsuarioService.GetByIdAsync(id);
            if (UsuaUsuario == null)
            {
                return NotFound();
            }

            await _UsuarioService.DeleteAsync(UsuaUsuario);

            return NoContent();
        }
    }
}
