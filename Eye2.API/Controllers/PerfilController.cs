using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Eye2.Domain.Entity;
using Eye2.Domain.DTO;
using Microsoft.AspNetCore.Authorization;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Exceptions;

namespace Eye2.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PerfilController : BaseController
    {
        private readonly IPerfilService _perfilService;

        public PerfilController(IPerfilService perfilService, IMapper mapper) : base(mapper)
        {
            _perfilService = perfilService;
        }

        // GET: api/Perfil
        [HttpGet]
        public ActionResult<IEnumerable<PerfilDTO>> GetPerfPerfil()
        {
            IEnumerable<PerfPerfil> PerfPerfils = _perfilService.AsQueryable();
            IEnumerable<PerfilDTO> perfils = mapper.Map<IEnumerable<PerfPerfil>, IEnumerable<PerfilDTO>>(PerfPerfils);
            return Ok(perfils);
        }

        // GET: api/Perfil/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PerfilDTO>> GetPerfPerfilAsync(long id)
        {
            PerfPerfil PerfPerfil = await _perfilService.GetByIdAsync(id);

            if (PerfPerfil == null)
            {
                return NotFound();
            }

            var perfil = mapper.Map<PerfilDTO>(PerfPerfil);
            return Ok(perfil);
        }

        // PUT: api/Perfil/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerfPerfil(long id, PerfilDTO perfilDto)
        {
            if (id != perfilDto.codigo)
            {
                return BadRequest();
            }

            PerfPerfil existingItem = await _perfilService.GetByIdAsync(id);
            if (existingItem is null)
            {
                return NotFound();
            }

            existingItem.PerfNome = perfilDto.nome;
            try
            {
                PerfPerfil result = await _perfilService.UpdateAsync(existingItem);
                return Ok(result);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        // POST: api/Perfil
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PerfilDTO>> PostPerfPerfilAsync(NovoPerfilDTO request)
        {
            try
            {
                PerfPerfil created = await _perfilService.CreateAsync(request.toClass());
                var perfil = mapper.Map<PerfilDTO>(created);
                return CreatedAtAction("GetPerfPerfil", new { id = created.PerfCodigo }, perfil);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        // DELETE: api/Perfil/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePerfPerfilAsync(long id)
        {
            var PerfPerfil = await _perfilService.GetByIdAsync(id);
            if (PerfPerfil == null)
            {
                return NotFound();
            }

            await _perfilService.DeleteAsync(PerfPerfil);

            return NoContent();
        }
    }
}
