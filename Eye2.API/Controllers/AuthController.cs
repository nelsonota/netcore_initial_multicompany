using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.DTO;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Eye2.Domain.Exceptions;

namespace Eye2.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {
        private ILoginService _loginService;
        private IUsuarioService _usuarioService;

        public AuthController(ILoginService loginService, IMapper mapper, IUsuarioService usuarioService) : base(mapper)
        {
            _loginService = loginService;
            _usuarioService = usuarioService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("signup")]
        public async Task<ActionResult<EmpresaDTO>> SignUpAsync([FromBody] NovaEmpresaDTO request)
        {
            try
            {
                EmprEmpresa created = await _loginService.CreateAccountAsync(request.toClass());
                var empresa = mapper.Map<EmpresaDTO>(created);
                return Ok(empresa);    
            }
            catch (ValidationException ex)
            {
                return UnprocessableEntity(ex.Failures);
            }
            
        }

        [HttpGet]
        [Authorize]
        [Route("start")]
        public async Task<ActionResult> StartSession()
        {
            string perf = "";
            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;
            var email = claimsIdentity.FindFirst("preferred_username")?.Value;
            if (email != null) {
                UsuaUsuario usuaUsuario = await _usuarioService.GetByEmailAsync(email);
                perf = usuaUsuario.UsuaPerfCodigo.ToString();
                HttpContext.Session.SetString("PerfCodigo", perf);
            }
            return Ok();
        }
    }
}