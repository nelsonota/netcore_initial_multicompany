﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Eye2.API.Controllers
{
    /// <summary>
    ///  Base controller
    /// </summary>
    public class BaseController : Controller
    {
        protected private readonly IMapper mapper;

        /// <summary>
        /// basecontroller constructor
        /// </summary>
        /// <param name="mapper"></param>
        public BaseController(IMapper mapper)
        {
            this.mapper = mapper;
        }
    }
}
