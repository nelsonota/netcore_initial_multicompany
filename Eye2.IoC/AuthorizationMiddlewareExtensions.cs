using Microsoft.AspNetCore.Builder;
using Eye2.Domain.Authorization;

namespace Eye2.IoC 
{
    public static class AuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            // Register the middleware using the UseMiddleware extension method
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}