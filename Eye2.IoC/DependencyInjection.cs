﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using FluentValidation;
using Eye2.FluentValidation.Extensions.AutoMapper;
using Eye2.Data.Context;
using Eye2.Data.Repositories;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Services;
using Eye2.Domain.Authorization;
using Eye2.Domain.AutoMapper;
using Eye2.Domain.Resources;
using Eye2.Domain.Validations;
using Keycloak.AuthServices.Authentication;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using AutoMapper;

namespace Eye2.IoC 
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(configuration);

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.Name = "Eye2.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddAutoMapper(typeof(AutoMapperProfile));

            services.AddHttpContextAccessor();
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            services.AddDbContext<DbPostgresContext>(options =>
                        options.UseNpgsql(configuration.GetConnectionString("Eye2DB")));
            #if DEBUG
                // Apenas para ambiente de desenvolvimento - Ignorar certificados inválidos
                var handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
                services.AddSingleton<HttpClient>(new HttpClient(handler));
            #else
                services.AddSingleton<HttpClient>();
            #endif
            
            services.AddRepositoryDependency();
            services.AddServiceDependency();

            services.AddValidators();

            return services;
        }
        private static void AddRepositoryDependency(this IServiceCollection services)
        {
            services.AddScoped<IEmpresaRepository, EmpresaRepository>();
            services.AddScoped<IPerfilRepository, PerfilRepository>();
            services.AddScoped<IUsuarioRepository, UsuariosRepository>();
            services.AddScoped<IKeycloakRepository, KeycloakRepository>();
            services.AddScoped<IPerfObjectRepository, PerfObjectRepository>();
        }

        private static void AddServiceDependency(this IServiceCollection services)
        {
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IEmpresaService, EmpresaService>();
            services.AddScoped<IPerfilService, PerfilService>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<AuthorizationMiddleware>();
        }

        private static void AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var authenticationOptions = configuration
                                .GetSection(KeycloakAuthenticationOptions.Section)
                                .Get<KeycloakAuthenticationOptions>();
            IdentityModelEventSource.ShowPII = true;

            services.AddKeycloakAuthentication(authenticationOptions, o => {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                o.BackchannelHttpHandler = httpClientHandler;

                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true
                };
                o.Events = new JwtBearerEvents()
                {
                    OnAuthenticationFailed = e =>
                    {
                        // Console.WriteLine("JWT Authentication Failed:");
                        Console.WriteLine(e.Exception);
                        e.NoResult();
                        e.Response.StatusCode = StatusCodes.Status401Unauthorized;

                        return Task.CompletedTask;
                    }
                };
            });
        }

        private static void AddValidators(this IServiceCollection services)
        {
            #region Resources
                services.AddLocalization(o => o.ResourcesPath = "Resources");
                services.AddSingleton<Resource>();
            #endregion Resources
            
            services.AddValidatorsFromAssemblyContaining<EmpresaValidation>();
            services.AddValidatorsFromAssemblyContaining<PerfilValidation>();
            services.AddValidatorsFromAssemblyContaining<UsuarioValidation>();
        }

        public static void ConfigureAutoMapperAndFluentValidation(WebApplication app)
        {
            var mapper = app.Services.GetRequiredService<IMapper>();
            mapper.ConfigurationProvider.AssertConfigurationIsValid();

            var resolver = new FluentValidationPropertyNameResolver(mapper);
            ValidatorOptions.Global.PropertyNameResolver = resolver.Resolve;
        }

        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder builder) {
            builder.UseSession();
            return builder;
        } 
    }
}
