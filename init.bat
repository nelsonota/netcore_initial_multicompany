// https://balta.io/blog/migrations-efcore-dotnet-postgresql

dotnet add package Microsoft.EntityFrameworkCore --version 5.0
dotnet add package Microsoft.EntityFrameworkCore.Design --version 5.0
dotnet add package Microsoft.EntityFrameworkCore.Tools --version 5.0
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 5.0

dotnet tool install --global dotnet-ef

dotnet ef dbcontext scaffold "Host=localhost;Database=eye2;Username=postgres;Password=postgres" Npgsql.EntityFrameworkCore.PostgreSQL -o Models -c MyDbContext

dotnet ef dbcontext scaffold "Host=localhost;Database=eye2;Username=postgres;Password=postgres" Npgsql.EntityFrameworkCore.PostgreSQL -o Models --context-dir Models

dotnet ef database update
// dotnet ef database update 0 (rollback)

dotnet tool install -g dotnet-aspnet-codegenerator --version 5.0
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 5.0

dotnet aspnet-codegenerator controller -name EmpresaController -async -api -m EmprEmpresa -dc MyDbContext -outDir Controllers -sqlite

dotnet add package Microsoft.AspNetCore.Authentication
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer --version 5.0
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson --version 5.0

dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection --version 8.1.1
dotnet add package FluentValidation.AspNetCore
dotnet add package FluentValidationExtensions.AutoMapper

dotnet add package NLog.Extensions.Logging
dotnet add package Microsoft.Extensions.DependencyInjection 
dotnet add package Microsoft.Extensions.Configuration.Json
dotnet add package NLog.Web.AspNetCore