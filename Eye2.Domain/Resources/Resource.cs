﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Localization;

namespace Eye2.Domain.Resources
{
    public class Resource
    {
        private readonly IStringLocalizer localizer;

        private readonly Assembly assembly;

        public Resource(IStringLocalizerFactory factory)
        {
            var type = typeof(Resource);
            this.assembly = type.GetTypeInfo().Assembly;
            localizer = factory.Create("Resource", assembly.GetName().Name);
        }

        public string GetMessage(string resourceName, params object[] args)
        {
            string res = localizer[resourceName].Value;

            if ((args ?? Array.Empty<object>()).Any())
            {
                return String.Format(res, args);
            }

            return res;
        }

        public string GetMessage(string resourceName, Dictionary<string, string> args)
        {
            string res = localizer[resourceName].Value;

            if (args.Any())
            {
                foreach (var item in args.Keys)
                {
                    res = res.Replace(item, args[item]);
                }
            }

            return res;
        }
    }
}
