using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Text.Json;

namespace Eye2.Domain.Authorization
{
    public class AuthorizationMiddleware : IMiddleware
    {
        private readonly IPermissionService _permissionService;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        public readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUsuarioService _usuarioService;

        public AuthorizationMiddleware(IPermissionService permissionService, IConfiguration configuration, HttpClient httpClient, IHttpContextAccessor httpContextAccessor, IUsuarioService usuarioService)
        {
            _permissionService = permissionService;
            _configuration = configuration;
            _httpClient = httpClient;
            _httpContextAccessor = httpContextAccessor;
            _usuarioService = usuarioService;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var endpoint = context.GetEndpoint();
            var cont = true;
            if (endpoint != null)
            {
                var allowAnonymousAttributeAttribute = endpoint.Metadata.GetMetadata<AllowAnonymousAttribute>();
                if (allowAnonymousAttributeAttribute != null)
                {
                    await next(context);
                    cont = false;
                }
            }
            if (cont) {
                // Verifique se o usuário está autenticado
                if (!context.User.Identity.IsAuthenticated)
                {
                    context.Response.StatusCode = 401; // Unauthorized
                    await context.Response.WriteAsync("Unauthorized");
                    return;
                }
                var authorizationHeader = context.Request.Headers["Authorization"].ToString();
                var accessToken = authorizationHeader.Substring("Bearer ".Length).Trim();
                var tokenAtivo = await IsTokenActiveAsync(accessToken);
                if (!tokenAtivo) {
                    context.Response.StatusCode = 401; // Unauthorized
                    await context.Response.WriteAsync("Unauthorized");
                    return;
                }

                string usuaCodigo = _httpContextAccessor.HttpContext?.Session.GetString("UsuaCodigo") ?? "";
                if (string.IsNullOrEmpty(usuaCodigo)) {
                    string email = _httpContextAccessor.HttpContext?.User.FindFirst(ClaimTypes.Email)?.Value ?? "";
                    if (!string.IsNullOrEmpty(email)) {
                        UsuaUsuario usuaUsuario = await this._usuarioService.GetByEmailAsync(email);
                        _httpContextAccessor.HttpContext?.Session.SetString("UsuaCodigo", usuaUsuario.UsuaCodigo.ToString());
                        _httpContextAccessor.HttpContext?.Session.SetString("EmprCodigo", usuaUsuario.PerfPerfil.EmprCodigoFK.ToString());
                    }                    
                }

                var authorizeAttribute = endpoint?.Metadata.GetMetadata<AuthorizeAttribute>();
                if (authorizeAttribute != null)
                {
                    await next(context);
                    cont = false;
                }

                if (cont) {
                    // Obtenha o perfil do usuário na sessão
                    var perfCodigo = _httpContextAccessor.HttpContext?.Session.GetString("PerfCodigo");
                    var routeData = context.GetRouteData();
                    if (routeData != null)
                    {
                        var controller = routeData.Values["controller"];
                        var action = routeData.Values["action"];

                        if (!_permissionService.HasPermission(int.Parse(perfCodigo), controller + "/" + action))
                        {
                            context.Response.StatusCode = 403; // Forbidden
                            await context.Response.WriteAsync("Forbidden");
                            return;
                        }
                    }
                    await next(context);
                }
                
            }
        }

        public async Task<bool> IsTokenActiveAsync(string token)
        {
            var authServerUrl = _configuration["Keycloak:auth-server-url"];
            var realm = _configuration["Keycloak:realm"];
            var introspectionEndpoint = $"{authServerUrl}/realms/{realm}/protocol/openid-connect/token/introspect";
            var clientId = _configuration["Keycloak:client-id"];
            var clientSecret = _configuration["Keycloak:client-secret"];

            var response = await _httpClient.PostAsync(introspectionEndpoint, new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", token),
                new KeyValuePair<string, string>("client_id", clientId),
                new KeyValuePair<string, string>("client_secret", clientSecret),
            }));

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            var json = JsonSerializer.Deserialize<Dictionary<string, object>>(responseContent);
            if (json.ContainsKey("active"))
            {
                bool active = ((JsonElement)json["active"]).GetBoolean();
                return active;
            }
            return false;
        }
    }
}
