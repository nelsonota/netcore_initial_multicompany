﻿using AutoMapper;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;

namespace Eye2.Domain.AutoMapper.Mappers
{
    /// <summary>
    /// Perfil mapper
    /// </summary>
    public static class UsuarioMapper
    {
        /// <summary>
        /// mapping rules LocaisMapper
        /// </summary>
        /// <param name="profile"></param>
        public static void Map(Profile profile)
        {
            profile.RecognizePrefixes("Usua");            
            profile.CreateMap<UsuaUsuario, UsuarioDTO>();
        }
    }
}
