﻿using AutoMapper;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;

namespace Eye2.Domain.AutoMapper.Mappers
{
    /// <summary>
    /// Empresa mapper
    /// </summary>
    public static class EmpresaMapper
    {
        /// <summary>
        /// mapping rules LocaisMapper
        /// </summary>
        /// <param name="profile"></param>
        public static void Map(Profile profile)
        {
            profile.RecognizePrefixes("Empr");            
            profile.CreateMap<EmprEmpresa, EmpresaDTO>()
                .ForMember(dto => dto.razao_social, opt => opt.MapFrom(model => model.EmprRazaoSocial));
        }
    }
}
