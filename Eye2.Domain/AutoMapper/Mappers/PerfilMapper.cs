﻿using AutoMapper;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;

namespace Eye2.Domain.AutoMapper.Mappers
{
    /// <summary>
    /// Perfil mapper
    /// </summary>
    public static class PerfilMapper
    {
        /// <summary>
        /// mapping rules LocaisMapper
        /// </summary>
        /// <param name="profile"></param>
        public static void Map(Profile profile)
        {
            profile.RecognizePrefixes("Perf");            
            profile.CreateMap<PerfPerfil, PerfilDTO>();
        }
    }
}
