﻿using AutoMapper;
using Eye2.Domain.AutoMapper.Mappers;

namespace Eye2.Domain.AutoMapper
{
    /// AutoMapperProfile 
    public class AutoMapperProfile : Profile
    {
        /// AutoMapperProfile constructor  
        public AutoMapperProfile()
        {
            EmpresaMapper.Map(this);
            PerfilMapper.Map(this);
            UsuarioMapper.Map(this);
        }
    }
}
