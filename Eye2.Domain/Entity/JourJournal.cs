﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eye2.Domain.Entity
{
    public partial class JourJournal
    {
        public JourJournal()
        {
            JdetJournalDetails = new HashSet<JdetJournalDetail>();
        }

        [Key]
        public long JourCodigo { get; set; }
        public string JourModel { get; set; }
        public long JourModelCodigo { get; set; }
        public string JourJsonData { get; set; }
        public DateTime JourCreated { get; set; }
        public long JourUsuaCodigo { get; set; }
        public long JourEmprCodigo { get; set; }

        public virtual ICollection<JdetJournalDetail> JdetJournalDetails { get; set; }
    }
}
