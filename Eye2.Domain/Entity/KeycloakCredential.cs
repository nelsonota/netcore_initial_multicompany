namespace Eye2.Domain.Entity
{
    public class KeycloakCredential
    {
        public string type { get; set; }
        public string value { get; set; }
        public bool temporary { get; set; }
    }
}
