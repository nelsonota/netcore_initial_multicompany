﻿using System;
using System.Collections.Generic;
using Eye2.Domain.Interfaces.Entity;

namespace Eye2.Domain.Entity
{
    public partial class EmprEmpresa: IDbPostgres, IHasTimestamps
    {
        
        public EmprEmpresa()
        {
        }
        
        public long EmprCodigo { get; set; }

        public string EmprDocumento { get; set; }

        public string EmprRazaoSocial { get; set; }

        public DateTime? Created { get; set; }
        
        public DateTime? Modified { get; set; }
        
        public DateTime? Deleted { get; set; }

        public ICollection<PerfPerfil> Perfis { get; set; }
    }
}
