using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Eye2.Domain.Interfaces.Entity;

namespace Eye2.Domain.Entity
{
    public class PerfPerfil: IDbPostgres, IHasTimestamps, IHasAccount
    {
        public long PerfCodigo { get; set; }
        public long EmprCodigoFK { get; set; }
        public string PerfNome { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public EmprEmpresa? EmprEmpresa { get; set; }

        [JsonIgnore]
        public ICollection<UsuaUsuario>? Usuarios { get; set; }

    }
}