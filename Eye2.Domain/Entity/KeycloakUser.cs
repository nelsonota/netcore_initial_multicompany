using System.Collections.Generic;

namespace Eye2.Domain.Entity
{
    public class KeycloakUser
    {
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public bool enabled { get; set; }
        public Dictionary<string, List<string>> attributes { get; set; }
        public ICollection<string> requiredActions { get; set; }
        public ICollection<string> realmRoles { get; set; }
        public ICollection<KeycloakCredential> credentials { get; set; }

        public static KeycloakUser FromUsuaUsuario(UsuaUsuario usuaUsuario)
        {
            return new KeycloakUser
            {
                username = usuaUsuario.UsuaEmail, // Pode ser o email ou outro identificador único
                firstName = usuaUsuario.UsuaNome,
                lastName = string.Empty, // Você pode querer ajustar isso com base na sua lógica
                email = usuaUsuario.UsuaEmail,
                enabled = true,
                attributes = new Dictionary<string, List<string>> // Configurando o atributo personalizado
                {
                    { "usuaCodigo", new List<string> { usuaUsuario.UsuaCodigo.ToString() } }
                },
                requiredActions = new List<string>
                {
                    {"VERIFY_EMAIL"}
                },
                realmRoles = new List<string>
                {
                    {"mb-user"}
                },
                // credentials = new List<KeycloakCredential>
                // {
                //     new KeycloakCredential
                //     {
                //         type = "password",
                //         value = usuaUsuario.UsuaSenha,
                //         temporary = false // Isso pode ser ajustado conforme necessário
                //     }
                // }
            };
        }
    }
}
