namespace Eye2.Domain.Entity
{
    public class PobjPerfObject: IDbPostgres
    {
        public long PobjCodigo { get; set; }

        public long PobjPerfCodigo { get; set; }

        public string? PobjObjeto { get; set; }

        public DateTime? Created { get; set; }
    }
}