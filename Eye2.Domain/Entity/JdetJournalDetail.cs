﻿using System.ComponentModel.DataAnnotations;

namespace Eye2.Domain.Entity
{
    public partial class JdetJournalDetail
    {
        [Key]
        public long JdetCodigo { get; set; }
        public long JdetJourCodigo { get; set; }
        public string JdetProperty { get; set; }
        public string JdetPropKey { get; set; }
        public string JdetOldValue { get; set; }
        public string JdetValue { get; set; }

        public virtual JourJournal JdetJourCodigoNavigation { get; set; }
    }
}
