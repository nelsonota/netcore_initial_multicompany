﻿using System;
using System.Security.Cryptography;
using System.Text;
using Eye2.Domain.Interfaces.Entity;

namespace Eye2.Domain.Entity
{
    public partial class UsuaUsuario: IDbPostgres, IHasTimestamps
    {
        public long UsuaCodigo { get; set; }
        public long UsuaPerfCodigo { get; set; }
        public string UsuaEmail { get; set; }
        public string UsuaNome { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public PerfPerfil PerfPerfil { get; set; }

    }
}
