using System.Linq;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Resources;
using FluentValidation;

namespace Eye2.Domain.Validations
{
    public class PerfilValidation : AbstractValidator<PerfPerfil>
    {
        public PerfilValidation(Resource resource, IEmpresaRepository empresasRepository) 
        {
            RuleFor(e => e.PerfNome)
               .NotEmpty()
               .WithMessage(resource.GetMessage("PropertyNotEmpty"));

            RuleFor(e => e.EmprCodigoFK)
                .MustAsync(async (item, numero, context) =>
                {
                    var exists = await empresasRepository.GetByIdAsync(item.EmprCodigoFK);
                    return exists != null;
                })
                .When(e => e.EmprCodigoFK > 0)
                .WithMessage(resource.GetMessage("PropertyNotExistsByValue"));
        }
    }
}
