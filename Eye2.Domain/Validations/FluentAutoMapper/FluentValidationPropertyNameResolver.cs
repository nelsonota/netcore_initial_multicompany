﻿using AutoMapper;
using AutoMapper.Internal;
using Eye2.FluentValidation.Extensions.AutoMapper.Validation;
using JetBrains.Annotations;
using System.Linq.Expressions;
using System.Reflection;

namespace Eye2.FluentValidation.Extensions.AutoMapper
{
    /// <summary>
    /// Implementation from IFluentValidationPropertyNameResolver
    /// </summary>
    /// <seealso cref="IFluentValidationPropertyNameResolver" />
    public class FluentValidationPropertyNameResolver : IFluentValidationPropertyNameResolver
    {
        private readonly IDictionary<(Type dtoType, string dtoProperty), string> _mappings = new Dictionary<(Type dtoType, string dtoProperty), string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="FluentValidationPropertyNameResolver"/> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        public FluentValidationPropertyNameResolver([NotNull] IMapper mapper)
        {
            Guard.NotNull(mapper, nameof(mapper));

            Init(mapper);
        }

        private void Init(IMapper mapper)
        {
            var allTypeMaps = mapper.ConfigurationProvider.Internal().GetAllTypeMaps();
            foreach (TypeMap map in allTypeMaps)
            {
                var propertyMaps = map.PropertyMaps;
                foreach (PropertyMap propertyMap in propertyMaps)
                {
                    string destinationName = propertyMap.DestinationName;
                    string modelMemberName = propertyMap.SourceMember.Name;

                    (Type, string) key = (map.SourceType, modelMemberName);
                    _mappings.Add(key, destinationName);
                }
            }
        }

        /// <inheritdoc cref="IFluentValidationPropertyNameResolver.Resolve(Type, MemberInfo, LambdaExpression)"/>
        public string Resolve(Type type, MemberInfo memberInfo, LambdaExpression lambdaExpression)
        {
            string propertyName = memberInfo.Name;

            (Type, string) key = (type, propertyName);

            // Not found in dictionary, just return memberInfo.Name or null.
            return _mappings.ContainsKey(key) ? _mappings[key] : propertyName;
        }
    }
}