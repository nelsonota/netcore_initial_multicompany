using System.Linq;
using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Resources;
using FluentValidation;

namespace Eye2.Domain.Validations
{
    public class UsuarioValidation : AbstractValidator<UsuaUsuario>
    {
        IUsuarioRepository _usuariosRepository;

        public UsuarioValidation(Resource resource, IPerfilRepository perfisRepository, IUsuarioRepository usuariosRepository) 
        {
            _usuariosRepository = usuariosRepository;

            RuleFor(e => e.UsuaEmail)
               .NotEmpty()
               .WithMessage(resource.GetMessage("PropertyNotEmpty"))
               .MustAsync(UniqueEmailAsync)
               .WithMessage(resource.GetMessage("PropertyExistsByValue"));

            RuleFor(e => e.UsuaPerfCodigo)
                .MustAsync(async (item, numero, context) =>
                {
                    if (item.UsuaPerfCodigo > 0)
                    {
                    var exists = await perfisRepository.GetByIdAsync(item.UsuaPerfCodigo);
                    return exists != null;
                    }
                    return true;
                })
                .When(e => e.UsuaPerfCodigo > 0)
                .WithMessage(resource.GetMessage("PropertyNotExistsByValue"));
        }

        private async Task<bool> UniqueEmailAsync(string email, CancellationToken cancellationToken)
        {
            return !await _usuariosRepository.HasEmailAsync(email);
        }
    }
}
