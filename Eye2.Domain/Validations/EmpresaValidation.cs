using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Resources;
using FluentValidation;

namespace Eye2.Domain.Validations
{
    public class EmpresaValidation : AbstractValidator<EmprEmpresa>
    {
        IEmpresaRepository _empresasRepository;
        public EmpresaValidation(Resource resource, IEmpresaRepository empresasRepository) 
        {
            _empresasRepository = empresasRepository;

            RuleFor(e => e.EmprRazaoSocial)
               .NotEmpty()
               .WithMessage(resource.GetMessage("PropertyNotEmpty"));

            RuleFor(e => e.EmprDocumento)
                .NotEmpty()
                .WithMessage(resource.GetMessage("PropertyNotEmpty"))
                .Must(UniqueDocumento)
                .WithMessage(resource.GetMessage("PropertyExistsByValue"));

        }

        private bool UniqueDocumento(string documento)
        {
            return !_empresasRepository.HasDocumento(documento);
        }
    }
}
