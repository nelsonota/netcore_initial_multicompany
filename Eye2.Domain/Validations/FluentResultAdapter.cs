﻿using Eye2.Domain.Exceptions;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Eye2.Domain.Validations
{
    public static class FluentResultAdapter
    {
        public static void CheckErrors(this ValidationResult result)
        {
            if (!(result?.IsValid ?? true))
            {
                var failures = result.Errors.Where(f => f != null).ToList();
                if (failures.Count > 0)
                    throw new ValidationException(failures);
            }
        }
    }
}
