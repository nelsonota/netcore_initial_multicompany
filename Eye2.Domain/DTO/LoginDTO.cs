namespace Eye2.Domain.DTO
{
        public record LoginDTO
        {
            public string email { get; set; }
            public string password { get; set; }
        }
}