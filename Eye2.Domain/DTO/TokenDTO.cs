using System;

namespace Eye2.Domain.DTO
{
    public class TokenDTO
    {
        public TokenDTO(bool authenticated, string created, string expiration, string accessToken, string refreshToken)
        {
            Authenticated = authenticated;
            Created = created;
            Expiration = expiration;
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }

        public Boolean Authenticated { get; set; }

        public string Created { get; set; }

        public string Expiration { get; set; }

        public string AccessToken { get; set; }
        
        public string RefreshToken { get; set; }
        
    }
}
