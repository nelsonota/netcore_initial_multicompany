using System;
using Eye2.Domain.Entity;

namespace Eye2.Domain.DTO
{
    public class UsuarioDTO
    {
        public long codigo { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public PerfilDTO? perfil { get; set; }

        public UsuaUsuario toClass() =>  new UsuaUsuario{ UsuaNome = this.nome, UsuaEmail = this.email, UsuaPerfCodigo = this.perfil.codigo };
    }
}
