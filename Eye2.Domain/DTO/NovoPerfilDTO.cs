using System;
using Eye2.Domain.Entity;

namespace Eye2.Domain.DTO
{
    public class NovoPerfilDTO
    {        
        public string nome { get; set; }

        public PerfPerfil toClass() =>  new PerfPerfil{ PerfNome = this.nome };
    }
}
