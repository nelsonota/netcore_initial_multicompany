using System;

namespace Eye2.Domain.DTO
{
    public class EmpresaDTO
    {        
        public long codigo { get; set; }

        public string documento { get; set; }

        public string razao_social { get; set; }

        public DateTime? created { get; set; }
        
        public DateTime? modified { get; set; }
        
    }
}
