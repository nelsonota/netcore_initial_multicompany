using System;
using System.Collections.Generic;

namespace Eye2.Domain.DTO
{
    public class PerfilDTO
    {
        public long codigo { get; set; }

        public string nome { get; set; }

        public DateTime? created { get; set; }

        public DateTime? modified { get; set; }
    }
}