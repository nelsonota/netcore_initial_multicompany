using System.Collections.Generic;
using Eye2.Domain.Entity;

namespace Eye2.Domain.DTO
{
        public record NovaEmpresaDTO
        {
            public string documento { get; set; }
            public string razaoSocial { get; set; }
            public string usuario { get; set; }
            public string email { get; set; }
            public string senha { get; set; }

            public EmprEmpresa toClass()
            {
                List<PerfPerfil> perfPerfis = new List<PerfPerfil>();
                List<UsuaUsuario> usuaUsuarios = new List<UsuaUsuario>();

                usuaUsuarios.Add(new UsuaUsuario { UsuaEmail = this.email, UsuaNome = this.usuario});
                PerfPerfil perfil = new PerfPerfil{ PerfNome = "Admin", Usuarios = usuaUsuarios };
                perfPerfis.Add(perfil);
                
                return new EmprEmpresa { EmprDocumento = this.documento, EmprRazaoSocial = this.razaoSocial, Perfis = perfPerfis };
            }
        }
}