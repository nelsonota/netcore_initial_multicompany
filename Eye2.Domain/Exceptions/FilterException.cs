﻿namespace Eye2.Domain.Exceptions
{
    [Serializable]
    public class FilterException : Exception
    {
        public FilterException(string message) : base(message) { }
    }
}
