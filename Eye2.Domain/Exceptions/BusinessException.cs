﻿using System;
using System.Runtime.Serialization;

namespace Eye2.Domain.Exceptions
{
    [Serializable]
    public class BusinessException : Exception
    {
        protected BusinessException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public BusinessException(string message) : base(message) { }
        public BusinessException(string message, Exception innerException) : base(message, innerException) { }
    }
}
