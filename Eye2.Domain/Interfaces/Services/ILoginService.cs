using System.Threading.Tasks;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Services
{
    public interface ILoginService
    {
        // TokenDTO ValidateCredentials(LoginDTO user);

        Task<EmprEmpresa> CreateAccountAsync(EmprEmpresa empresa);
    }
}