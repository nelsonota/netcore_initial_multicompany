using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Services
{
    public interface IPerfilService
    {
        Task<PerfPerfil> CreateAsync(PerfPerfil perfPerfil);
        Task DeleteAsync(PerfPerfil perfPerfil);
        Task<PerfPerfil> GetByIdAsync(long id);
        IQueryable<PerfPerfil> AsQueryable();
        Task<PerfPerfil> UpdateAsync(PerfPerfil existingItem);
    }
}