namespace Eye2.Domain.Interfaces.Services
{
    public interface IPermissionService
    {
        bool HasPermission(int perfCodigo, string controllerMetodo);
    }
}