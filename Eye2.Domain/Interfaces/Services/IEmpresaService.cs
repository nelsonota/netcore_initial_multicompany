using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Services
{
    public interface IEmpresaService
    {
        Task DeleteAsync(EmprEmpresa emprEmpresa);
        Task<EmprEmpresa> GetByIdAsync(long id);
        IQueryable<EmprEmpresa> AsQueryable();
        Task<EmprEmpresa> UpdateAsync(EmprEmpresa existingItem);
    }
}