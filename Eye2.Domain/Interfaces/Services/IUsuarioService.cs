using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Services
{
    public interface IUsuarioService
    {
        Task<UsuaUsuario> GetByEmailAsync(string email);
        Task<UsuaUsuario> CreateAsync(UsuaUsuario usuaUsuario);
        Task DeleteAsync(UsuaUsuario usuaUsuario);
        Task<UsuaUsuario> GetByIdAsync(long id);
        IQueryable<UsuaUsuario> AsQueryable();
        Task<UsuaUsuario> UpdateAsync(UsuaUsuario existingItem);
    }
}