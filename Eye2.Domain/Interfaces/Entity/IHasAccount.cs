using System;


namespace Eye2.Domain.Interfaces.Entity
{
    public interface IHasAccount
    {
        long EmprCodigoFK { get; set; }
    }
}