using System;

namespace Eye2.Domain.Interfaces.Entity
{
    public interface IHasTimestamps
    {
        DateTime? Created { get; set; }
        DateTime? Deleted { get; set; }
        DateTime? Modified { get; set; }
    }
}