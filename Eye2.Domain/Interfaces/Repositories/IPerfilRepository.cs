using System.Collections.Generic;
using System.Threading.Tasks;
using Eye2.Domain.Interface.Repositories;
using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Repositories
{
    public interface IPerfilRepository: IBaseRepository<PerfPerfil> 
    {
    }
}