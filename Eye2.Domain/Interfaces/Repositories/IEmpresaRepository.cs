using System.Collections.Generic;
using System.Threading.Tasks;
using Eye2.Domain.Interface.Repositories;
using Eye2.Domain.Entity;
using System;
using Microsoft.EntityFrameworkCore.Storage;

namespace Eye2.Domain.Interfaces.Repositories
{
    public interface IEmpresaRepository: IBaseRepository<EmprEmpresa> 
    {
        public IDbContextTransaction BeginTransaction();
        public bool HasDocumento(string documento);
    }
}