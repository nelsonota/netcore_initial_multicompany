﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eye2.Domain.Entity;

namespace Eye2.Domain.Interface.Repositories
{
    public interface IBaseRepository<TEntity>
        where TEntity : IEntity
    {
        IQueryable<TEntity> AsQueryable();

        Task<TEntity> GetByIdAsync(long Id, bool noTracking = false);
        Task<TEntity> CreateAsync(TEntity entity);
        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="disconectedScenario">When entity isn't tracked by DbContext</param>
        /// <returns></returns>
        Task<TEntity> UpdateAsync(TEntity entity, bool disconectedScenario = true);
        /// <summary>
        /// Update list entity
        /// </summary>
        /// <param name="listEntity"></param>
        /// <param name="disconectedScenario">When entity isn't tracked by DbContext</param>
        /// <returns></returns>
        Task<List<TEntity>> UpdateAsync(List<TEntity> listEntity, bool disconectedScenario = true);
        Task<int> DeleteAsync(TEntity entity);
        Task<int> DeleteAsync(List<TEntity> entity);
    }
}
