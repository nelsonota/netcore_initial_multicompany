using System.Collections.Generic;
using System.Threading.Tasks;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;
using Eye2.Domain.Interface.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace Eye2.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository: IBaseRepository<UsuaUsuario> 
    {
        Task<UsuaUsuario> GetByEmailAsync(string email);
        Task<bool> HasEmailAsync(string email);
        public IDbContextTransaction BeginTransaction();
    }
}