using Eye2.Domain.Entity;

namespace Eye2.Domain.Interfaces.Repositories
{
    public interface IKeycloakRepository
    {
        public Task CreateUser(KeycloakUser user);
        public Task DeleteUser(string email);
    }
}