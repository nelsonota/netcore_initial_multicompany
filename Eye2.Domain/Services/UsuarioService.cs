using System.Security.Claims;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;
using Eye2.Domain.Configurations;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Validations;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore;

namespace Eye2.Domain.Services
{
    public class UsuarioService : IUsuarioService
    {
        private IUsuarioRepository _usuariosRepository;
        private UsuarioValidation _usuarioValidation;
        private IKeycloakRepository _keycloakRepository;

        public UsuarioService(IUsuarioRepository usuariosRepository, UsuarioValidation UsuarioValidation, IKeycloakRepository keycloakRepository)
        {
            _usuariosRepository = usuariosRepository;
            _usuarioValidation = UsuarioValidation;
            _keycloakRepository = keycloakRepository;
        }

        public Task<UsuaUsuario> GetByEmailAsync(string email)
        {
            return _usuariosRepository.GetByEmailAsync(email);
        }

        public async Task<UsuaUsuario> CreateAsync(UsuaUsuario UsuaUsuario)
        {
            using (var transaction = _usuariosRepository.BeginTransaction())
            {
                try
                {
                    var usuaUsuario = await _usuariosRepository.CreateAsync(UsuaUsuario);
                    await _keycloakRepository.CreateUser(KeycloakUser.FromUsuaUsuario(usuaUsuario));
                    transaction.Commit();
                    return usuaUsuario;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                
            }
        }

        public async Task DeleteAsync(UsuaUsuario UsuaUsuario)
        {
             using (var transaction = _usuariosRepository.BeginTransaction())
            {
                try
                {
                    var usuaUsuario = await _usuariosRepository.DeleteAsync(UsuaUsuario);
                    await _keycloakRepository.DeleteUser(UsuaUsuario.UsuaEmail);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        public Task<UsuaUsuario> GetByIdAsync(long id)
        {
            return _usuariosRepository.GetByIdAsync(id);
        }

        public IQueryable<UsuaUsuario> AsQueryable()
        {
            return _usuariosRepository.AsQueryable().Include(u => u.PerfPerfil);
        }

        public Task<UsuaUsuario> UpdateAsync(UsuaUsuario existingItem)
        {
            return _usuariosRepository.UpdateAsync(existingItem);
        }

        IQueryable<UsuaUsuario> IUsuarioService.AsQueryable()
        {
            return _usuariosRepository.AsQueryable();
        }
    }
}