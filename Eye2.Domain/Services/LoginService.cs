using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Validations;

namespace Eye2.Domain.Services
{
    public class LoginService : ILoginService
    {
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        // private TokenConfiguration _configuration;
        private IUsuarioRepository _usuariosRepository;
        private IEmpresaRepository _empresasRepository;
        // private ITokenService _tokenService;
        private IKeycloakRepository _keycloakRepository;
        private EmpresaValidation _empresaValidation;
        private UsuarioValidation _usuarioValidation;

        public LoginService(IUsuarioRepository usuariosRepository, IEmpresaRepository empresasRepository, EmpresaValidation empresaValidation, UsuarioValidation usuarioValidation, IKeycloakRepository keycloakRepository)
        {
            _usuariosRepository = usuariosRepository;
            _empresasRepository = empresasRepository;
            _keycloakRepository = keycloakRepository;
            _empresaValidation = empresaValidation;
            _usuarioValidation = usuarioValidation;
        }

        public async Task<EmprEmpresa> CreateAccountAsync(EmprEmpresa novaEmpresa)
        {
            var result = _empresaValidation.Validate(novaEmpresa);
            result.CheckErrors();
            UsuaUsuario usuaUsuario = novaEmpresa.Perfis.First().Usuarios.First();
            result = await _usuarioValidation.ValidateAsync(usuaUsuario);
            result.CheckErrors();
            
            using (var transaction = _empresasRepository.BeginTransaction())
            {
                try{
                    EmprEmpresa empresa = await _empresasRepository.CreateAsync(novaEmpresa);
                    await _keycloakRepository.CreateUser(KeycloakUser.FromUsuaUsuario(usuaUsuario));
                    transaction.Commit();
                    return empresa;
                } catch (Exception ex) {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}