using System.Security.Cryptography.X509Certificates;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;

namespace Eye2.Domain.Services
{
    public class PermissionService : IPermissionService
    {
        private IPerfObjectRepository _perfObjectRepository;

        public PermissionService(IPerfObjectRepository perfObjectRepository)
        {
            _perfObjectRepository = perfObjectRepository;
        }

        public bool HasPermission(int perfCodigo, string controllerMetodo)
        {
            var result = _perfObjectRepository.AsQueryable();
            result = result.Where(x => (x.PobjPerfCodigo == perfCodigo && x.PobjObjeto == controllerMetodo));
            return result.Count() > 0;
        }
    }
}
