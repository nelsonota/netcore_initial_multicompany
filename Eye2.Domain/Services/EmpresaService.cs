using System.Security.Claims;
using Eye2.Domain.DTO;
using Eye2.Domain.Entity;
using Eye2.Domain.Configurations;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Validations;
using System.IdentityModel.Tokens.Jwt;

namespace Eye2.Domain.Services
{
    public class EmpresaService : IEmpresaService
    {
        private IEmpresaRepository _empresasRepository;
        private EmpresaValidation _empresaValidation;

        public EmpresaService(IEmpresaRepository empresasRepository, EmpresaValidation empresaValidation)
        {
            _empresasRepository = empresasRepository;
            _empresaValidation = empresaValidation;
        }

        public Task DeleteAsync(EmprEmpresa emprEmpresa)
        {
            return _empresasRepository.DeleteAsync(emprEmpresa);
        }

        public Task<EmprEmpresa> GetByIdAsync(long id)
        {
            return _empresasRepository.GetByIdAsync(id);
        }

        public IQueryable<EmprEmpresa> AsQueryable()
        {
            return _empresasRepository.AsQueryable();
        }

        public Task<EmprEmpresa> UpdateAsync(EmprEmpresa existingItem)
        {
            return _empresasRepository.UpdateAsync(existingItem);
        }
    }
}