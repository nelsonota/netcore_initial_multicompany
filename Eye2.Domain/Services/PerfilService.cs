using Eye2.Domain.Entity;
using Eye2.Domain.Interfaces.Repositories;
using Eye2.Domain.Interfaces.Services;
using Eye2.Domain.Validations;

namespace Eye2.Domain.Services
{
    public class PerfilService : IPerfilService
    {
        private IPerfilRepository _PerfilRepository;
        private PerfilValidation _PerfilValidation;

        public PerfilService(IPerfilRepository PerfilsRepository, PerfilValidation PerfilValidation)
        {
            _PerfilRepository = PerfilsRepository;
            _PerfilValidation = PerfilValidation;
        }

        public Task<PerfPerfil> CreateAsync(PerfPerfil perfPerfil)
        {
            return _PerfilRepository.CreateAsync(perfPerfil);
        }

        public Task DeleteAsync(PerfPerfil perfPerfil)
        {
            return _PerfilRepository.DeleteAsync(perfPerfil);
        }

        public Task<PerfPerfil> GetByIdAsync(long id)
        {
            return _PerfilRepository.GetByIdAsync(id);
        }

        public IQueryable<PerfPerfil> AsQueryable()
        {
            return _PerfilRepository.AsQueryable();
        }

        public Task<PerfPerfil> UpdateAsync(PerfPerfil existingItem)
        {
            return _PerfilRepository.UpdateAsync(existingItem);
        }

        IQueryable<PerfPerfil> IPerfilService.AsQueryable()
        {
            return _PerfilRepository.AsQueryable();
        }
    }
}